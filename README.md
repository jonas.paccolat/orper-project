# Microbial mat thickness measurements project

This project contains microbial mat thickness measurements from a series of
flume experiments carried in Alpine environment and numerical tools to process
and analyze these data

### Data storage
The data folder in the root directory contains a folder per experiment, labeled
as *run1*, *run2* and *run3*. For each run, the raw data is stored in the folder
./data/\<runname\>/mat/raw as .txt files. The .txt file name correspond to the
measurement time with format *YYYY-MM-DDThh:mm.txt*.

Parameters of the run are stored in the file /data/\<runname\>/para.json

### Data processing
The data can be processed with the command `python process/main.py` from the root directory. The file /data/\<runname\>/mat/mat.txt containing the mat thickness timeseries is then generated.

The methods used to processing the data are gathered in the modules *process/timing.py* and *process/mat.py*

### Data analysis
The times series are categorized depending on the underlying sediment type. For
each type a logistic fit is applied. To run the analysis and generated a figure
in the folder /output use the command `python fit/main.py`
