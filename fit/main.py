#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to fit the mat thickness timeseries.

The timeseries are categorized on the basis of the sediment type (fine or
coarse) and a fit is computed for each category.

A plot is generated in the output directory.

Instructions
------------
- The path to the database folder containing the different run folders must be
specified in the *Global variables* below.
- The list of considered runs (names of associated folders) must be specified in
the *Global variables* below.
- To run the script, run: python main.py
"""

__author__ = "Jonas Paccolat"
__contact__ = "jonas.paccolat@epfl.ch"
__date__ = "2023/09/14"   ### Date it was last modified
__status__ = "Production" ### Production = still being developed. Else: Concluded/Finished.


####################
# Review History   #
####################


####################
# Libraries        #
####################

# Standard imports  ### (https://docs.python.org/3/library/)
import os
import json
from pathlib import Path

# Third party imports
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib.patches import Patch

# Internal imports

####################
# Global variables #
####################

# path to database directory
dtb_path = "/home/jonas/Documents/river/conferences/ORPER/orper-project/data/"
# list of runs to process
runs = ['run1', 'run2', 'run3']

####################
# Methods          #
####################

def create_mat_timeseries_dict(dtb_path, runs):
    """
    Load the mat timeseries of each run and save them in a dictionnary with
    information on the run

    Parameters
    ----------
    dtb_path:   (str)
        Path to the database containing the different run folders.
    runs:       (list)
        List of run folder names.
    """

    dtb_path = Path(dtb_path)

    all_mat_timeseries = dict()
    # loop over run folders
    for run in runs:
        run_path = dtb_path / run

        # load run parameters
        with open(run_path / "para.json") as f:
            para = json.load(f)

        # load mat timeseries
        data = np.loadtxt(
            run_path / "mat/mat.txt",
            delimiter=' ',
            dtype=str
        ).T

        t = np.array(data[1], dtype=float)
        w, dw = np.array(data[2:], dtype=float)
        w[1:] += para['sediments']['roughness']

        all_mat_timeseries[run] = {}
        all_mat_timeseries[run]['roughness'] = para['sediments']['roughness']
        all_mat_timeseries[run]['t'] = t
        all_mat_timeseries[run]['w'] = w
        all_mat_timeseries[run]['dw'] = dw
        all_mat_timeseries[run]['sediment'] = para['sediments']['name']

    return all_mat_timeseries

def group_mat_timeseries_per_sediment(all_mat_timeseries, sediment):
    """Select mat timeseries of a given sediment type and concatenate them."""

    t, w, dw = [], [], []
    for key, mat_timeseries in all_mat_timeseries.items():
        if mat_timeseries['sediment'] == sediment:
            t = np.concatenate([t, mat_timeseries['t'][1:]])
            w = np.concatenate([w, mat_timeseries['w'][1:]])
            dw = np.concatenate([dw, mat_timeseries['dw'][1:]])

    return t, w, dw

def fit(t, w, dw, func, method=None):
    """
    Fit the mat thickness timeseries with a given function

    Parameters
    ----------
    t:      (array)
        Array of measurment runtimes.
    w:      (array)
        Array of mat thicknesses.
    dw:     (array)
        Array of mat thickness uncertainties.
    func:   (str)
        Function to fit. Must either be 'logistic', 'linear' or 'logarithmic'.
    method: (str, optional)
        Method to apply to the fit algorithm. See *scipy.optimize.curve_fit*
        documentation. If None the default method is applied. Default is None.
    """

    if func == "logarithmic":

        p0 = None
        bounds = (-np.inf, np.inf)
        if method is None: method = 'lm'

        def f(t, wstar, tau):
            return wstar * np.log(1 + t / tau)

    if func == "linear":
        p0 = None
        bounds = (0, np.inf)
        if method is None: method = 'trf'

        def f(t, a):
            return a * t

    if func == "logistic":
        p0 = [w[-1], 1e-8, 24 * 60]
        bounds = (0, np.inf)
        if method is None: method = 'trf'

        def f(t, wstar, w0, tau):
            return wstar / (1 + (wstar / w0 - 1) * np.exp(-t / tau))

    popt, pcov = curve_fit(f, t, w, sigma=dw, method=method, p0=p0, bounds=bounds)
    perr = np.sqrt(np.diag(pcov))

    f_mat = lambda t: f(t, *popt)

    return f_mat, popt, perr

def plot(all_mat_timeseries, f_mat_fine, f_mat_coarse):
    """
    Plot the mat thickness timeseries together with the fitted function.
    An output directory is created in the root folder to save the generated
    figure.
    """

    plt.style.use(Path(os.path.dirname(__file__)) / "conf" / "plot_style.txt")

    legend = [
        Patch(color='C0', label='Fine'),
        Patch(color='C1', label='Coarse')
    ]

    fig, ax = plt.subplots()

    # plot fines
    mat_timeseries = all_mat_timeseries['run1']
    ax.errorbar(
            mat_timeseries['t'][1:],
            1e3 * mat_timeseries['w'][1:],
            yerr=1e3 * mat_timeseries['dw'][1:],
            ls='None', marker='o', ms=6, mfc=f'C0', mec='k',
            ecolor='k', capsize=4, capthick=1, elinewidth=1
        )

    mat_timeseries = all_mat_timeseries['run2']
    ax.errorbar(
            mat_timeseries['t'][1:],
            1e3 * mat_timeseries['w'][1:],
            yerr=1e3 * mat_timeseries['dw'][1:],
            ls='None', marker='s', ms=6, mfc=f'C0', mec='k',
            ecolor='k', capsize=4, capthick=1, elinewidth=1
        )

    x = np.linspace(0, 24 * 60 * 22)
    y = f_mat_fine(x)
    ax.plot(x, 1e3 * y, ls='--', c='k')

    # plot coarse
    mat_timeseries = all_mat_timeseries['run3']
    ax.errorbar(
            mat_timeseries['t'][1:],
            1e3 * mat_timeseries['w'][1:],
            yerr=1e3 * mat_timeseries['dw'][1:],
            ls='None', marker='o', ms=6, mfc=f'C1', mec='k',
            ecolor='k', capsize=4, capthick=1, elinewidth=1
        )

    x = np.linspace(0, 24 * 60 * 22)
    y = f_mat_coarse(x)
    ax.plot(x, 1e3 * y, ls='--', c='k')


    ax.set_xlabel('time [day]', fontsize=20)
    ax.set_ylabel('mat thickness [mm]', fontsize=20)

    l = np.arange(0, 37, 3)
    ax.set_xticks(60 * 24 * l)
    ax.set_xticklabels(l)
    ax.tick_params(labelsize=16)

    plt.legend(handles=legend, loc='upper left', fontsize=14)
    plt.tight_layout()

    # save figure into output folder
    path_to_output = Path(dtb_path).parent / "output"
    os.mkdir(path_to_output)
    plt.savefig(path_to_output / "mat-thickness.pdf")
    print(f"Mat thickness plot saved in {path_to_output}")

def main():

    print("\n[Start] Fit mat thickness timeseries")

    # create a dictionary with a mat timeseries per run
    all_mat_timeseries = create_mat_timeseries_dict(dtb_path, runs)

    # concatenate mat timeseries with fine sediments
    fine_mat_timeseries = group_mat_timeseries_per_sediment(
                                                all_mat_timeseries, 'fine')
    # fit a logistic function to the fine sediment timeseries
    f_mat_fine, popt_fine, perr_fine = fit(
                            *fine_mat_timeseries, 'logistic')

    # concatenate mat timeseries with coarse sediments
    coarse_mat_timeseries = group_mat_timeseries_per_sediment(
                                                all_mat_timeseries, 'coarse')
    # fit a logistic function to the coarse sediment timeseries
    f_mat_coarse, popt_coarse, perr_coarse = fit(
                            *coarse_mat_timeseries, 'logistic')

    # plot all mat timeseries together with the logistic fits and save it in the
    # output folder

    plot(all_mat_timeseries, f_mat_fine, f_mat_coarse)

    print("[End] Fit mat thickness timeseries\n")


if __name__ == "__main__":
    main()
