#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module to manage data acquisition time.

Methods to obtain the list of measurments times from raw data in the
np.datetime64 format (YYYY-MM-DDThh:mm:ss) and convert it to spantimes, namely
integers of time unit since a reference datetime.
"""

__author__ = "Jonas Paccolat"
__contact__ = "jonas.paccolat@epfl.ch"
__date__ = "2023/09/14"   ### Date it was last modified
__status__ = "Production" ### Production = still being developed. Else: Concluded/Finished.


####################
# Review History   #
####################


####################
# Libraries        #
####################

# Standard imports  ### (https://docs.python.org/3/library/)
import json

# Third party imports
import numpy as np

def set_reference_time_in_run_parameters(run_path):
    """
    Find start and end time from raw data and save it in the file *para.json*.

    Parameters
    ----------
    run_path:   (str)
        Path to the folder to search.
    """

    # load all measurment times for the given run directory
    times = load_measurment_times_from_dir(
            run_path / "mat" / "raw", unit='m', suffix='.txt')

    # set start and end times in the parameter file
    with open(run_path / "para.json") as f:
        para = json.load(f)

    para['run'] = {
        "start": str(times[0]),
        "end": str(times[-1]),
    }

    with open(run_path / "para.json", "w") as f:
        json.dump(para, f)

    print(f"Set reference times in {run_path / 'para.json'}")

def get_starttime(run_path, unit='m'):
    """
    Get experiment start time and associated timespan.

    Parameters
    ----------
    run_path:   (str)
        Path to the folder to search
    unit:       (str, optional)
        Time unit following np.datetime64 convention. Default is minutes, 'm'.
    """

    with open(run_path / "para.json") as f:
        para = json.load(f)

    starttime = np.datetime64(para['run']['start'], 'm')
    starttimespan = convert_times_to_spantimes(starttime, unit=unit)

    return starttimespan, starttime

def load_measurment_times_from_dir(
        path, start=None, end=None, unit='m', suffix='.json'):
    """
    Return the sorted list of measurement datetimes in a given folder.

    Parameters
    ----------
    path:   (str)
        Path to the folder to search
    start:  (str, optional)
        Earliest date in the output list. Format must be YYYY-MM-DD. If None,
        no limit is imposed. Default is None.
    end:    (str, optional)
        Latest date in the output list. Format must be YYYY-MM-DD. If None, no
        limit is imposed. Default is None.
    unit:  (str, optional)
        Time unit following np.datetime64 convention. Default is minutes, 'm'.
    suffix: (str, optional)
        Suffix of files to be searched for. Default is '.json'.

    Returns
    -------
    times: Sorted list of datetimes.
    """

    times = []
    for file in path.iterdir():
        if file.suffix == suffix:
            times.append(file.stem)

    times = np.array(times, dtype=f'datetime64[{unit}]')
    times.sort()

    if start is not None:
        times = times[times >= np.datetime64(start)]
    if end is not None:
        times = times[times <= np.datetime64(end)]

    return times

def convert_times_to_spantimes(times, unit='m'):
    """
    Convert list of datetimes to spantimes of given unit.

    Parameters
    ----------
    times:  (list)
        List of datetimes.
    unit:  (str, optional)
        Time unit following the np.datetime64 convention. Default is minutes,
        'm'.
    """

    times_with_correct_unit = np.array(times, dtype=f'datetime64[{unit}]')
    spantimes = np.array(times_with_correct_unit, dtype=int)

    return spantimes

def convert_timespans_to_times(timespans, unit):
    """
    Convert list of timespans with given unit to datetimes

    Parameters
    ----------
    timespans:  (list)
        List of timespans.
    unit:       (str)
        Time unit of the given timespan list following the np.datetime64
        convention.
    """

    times = np.array(timespans, dtype=f'datetime64[{unit}]')

    return times

def convert_times_to_runtimes(run_path, times, unit='m'):
    """
    Convert list of datetimes to runtimes (spantime since starttime)

    Parameters
    ----------
    run_path:   (str)
        Path to the run folder.
    times:      (list)
        List of datetimes.
    unit:       (str, optional)
        Time unit following the np.datetime64 convention. Default is minutes,
        'm'.
    """

    starttimespan, starttime = get_starttime(run_path, unit=unit)
    runtime = convert_times_to_spantimes(times, unit=unit) - starttimespan

    return runtime
