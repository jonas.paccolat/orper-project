#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to process the mat raw data.

Contains processing of the measurment times and the mat thicknesses.

Instructions
------------
- The path to the database folder containing the different run folders must be
specified in the *Global variables* below.
- The list of considered runs (names of associated folders) must be specified in
the *Global variables* below.
- To run the script, run: python main.py
"""

__author__ = "Jonas Paccolat"
__contact__ = "jonas.paccolat@epfl.ch"
__date__ = "2023/09/14"   ### Date it was last modified
__status__ = "Production" ### Production = still being developed. Else: Concluded/Finished.


####################
# Review History   #
####################


####################
# Libraries        #
####################

# Standard imports  ### (https://docs.python.org/3/library/)
import json
from pathlib import Path

# Third party imports

# Internal imports
import timing
import mat

####################
# Global variables #
####################

# path to database directory
dtb_path = "/home/jonas/Documents/river/conferences/ORPER/orper-project/data/"
# list of runs to process
runs = ['run1', 'run2', 'run3']

####################
# Methods          #
####################

def set_reference_time_in_run_parameters(dtb_path, runs):
    """
    Update the parameter file (*para.json*) of each run folder with start and
    end measurment times.

    Parameters
    ----------
    dtb_path:   (str)
        Path to the database containing the different run folders.
    runs:       (list)
        List of run folder names.
    """

    for run in runs:
        run_path = Path(dtb_path) / run
        timing.set_reference_time_in_run_parameters(run_path)

def process_mat_raw_data(dtb_path, runs):
    """
    Process mat raw data of each run folder and save processed timeseries in the
    files *<runname>/mat.txt*.

    Parameters
    ----------
    dtb_path:   (str)
        Path to the database containing the different run folders.
    runs:       (list)
        List of run folder names.
    """

    for run in runs:
        run_path = Path(dtb_path) / run

        with open(run_path / "para.json") as f:
            para = json.load(f)

        mat.process_raw_data(
            run_path=run_path,
            h_thread=para["mat"]["h_thread"],
            delta_w=para["mat"]["delta_w"]
        )

def main():

    print("\n[Start] Set reference time")
    set_reference_time_in_run_parameters(dtb_path, runs)
    print("[End] Set reference time \n")

    print("\n[Start] process mat raw data")
    process_mat_raw_data(dtb_path, runs)
    print("[End] process mat raw data\n")


if __name__ == "__main__":
    main()
