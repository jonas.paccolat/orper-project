#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module to process mat thickness data
"""

__author__ = "Jonas Paccolat"
__contact__ = "jonas.paccolat@epfl.ch"
__date__ = "2023/09/14"   ### Date it was last modified
__status__ = "Production" ### Production = still being developed. Else: Concluded/Finished.


####################
# Review History   #
####################


####################
# Libraries        #
####################

# Standard imports  ### (https://docs.python.org/3/library/)
import json
from pathlib import Path

# Third party imports
import numpy as np
from scipy.optimize import curve_fit

# Internal imports
import timing

def process_raw_data(run_path: str, delta_w: float, h_thread: float):
    """
    Process the raw mat data in a given folder and store the mat thickness
    timeseries in the file *mat.txt*.

    Parameters
    ----------
    run_path:   (str)
        Path to the run folder.
    delta_w:    (float)
        Mat thickness measurment uncertainty, in [m].
    h_thread:   (float)
        Screw thread height, in [m].
    """

    path_to_raw_data = run_path / "mat" / "raw"

    # load raw ata
    times = timing.load_measurment_times_from_dir(
                        path_to_raw_data, unit='m', suffix='.txt')
    number_of_turns = []
    for time in times:
        number_of_turns.append(
                np.loadtxt(
                    (path_to_raw_data / str(time)).with_suffix('.txt'),
                    delimiter=','
                    )
                )

    print(f"{len(times)} mat files loaded btw. {times[0]} and {times[-1]}")

    # process data
    w_mean, w_unc = convert_turns_to_meters(number_of_turns, delta_w, h_thread)
    runtimes = timing.convert_times_to_runtimes(run_path, times)

    # store times and mat thickness in a txt file
    np.savetxt(
        path_to_raw_data.parent / "mat.txt",
        np.array([times, runtimes, w_mean, w_unc], dtype=str).T,
        fmt='%.20s',
        header="datetime, runtime [min], mat th. [m], uncert. [m]"
    )
    print(f"Mat data stored in {path_to_raw_data.parent / 'mat.txt'}")

def convert_turns_to_meters(number_of_turns, delta_w, h_thread):
    """
    Convert number of turns to meters according to the given screw thread and
    compute the mean and standard deviation.

    Parameters
    ----------
    number_of_turns:    (float)
        Number of screw turns to reach the mat surface.
    delta_w:    (float)
        Mat thickness measurment uncertainty, in [m].
    h_thread:   (float)
        Screw thread height, in [m].

    Returns
    -------
    w_mean: (float)
        Mat thickness mean.
    w_unc: (float)
        Mat thickness uncertainty.
    """

    w = h_thread * (number_of_turns[0] - number_of_turns)
    w_mean = w.mean(axis=(1, 2))
    w_std = w.std(axis=(1, 2))
    w_unc = np.array([max(dw, delta_w) for dw in w_std])

    return w_mean, w_unc
